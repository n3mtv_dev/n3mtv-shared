-- HERE GOES YOUR CLIENT FUNCTIONALITY!
local nbPolice = 0

RegisterNetEvent('blanchi:drawTransform')
AddEventHandler('blanchi:drawTransform', function (qte)
		if(qte == nil) then
			qte = 0
		end
		if qte > 0 then

			ClearPrints()
			SetTextEntry_2("STRING")
			AddTextComponentString("~g~Vous avez blanchis votre argent")
			DrawSubtitleTimed(2000, 1)
		else
			-- ClearPrints()
			-- SetTextEntry_2("STRING")
			-- AddTextComponentString("~r~Vous n'avez plus d'argent sale")
			-- DrawSubtitleTimed(2000, 1)
		end
end)

local checksale=false
local blanchiment=false
local secondsRemaining=0
local timerfinish=false
local start=false

RegisterNetEvent('blanchi:resultcheck')
AddEventHandler('blanchi:resultcheck', function ()
	checksale = true
end)

Citizen.CreateThread(function()
    while true do
       Citizen.Wait(0)
       playerPed = GetPlayerPed(-1)
		local pos = GetEntityCoords(GetPlayerPed(-1), true)
		if pos then
				if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), 595.945,-457.364,24.8388, true) <= 2 then
					TriggerServerEvent("blanci:Checksale")
					if checksale == true then
						--if nbPolice > 3 then


							if checksale == true then
								ClearPrints()
								SetTextEntry_2("STRING")
								TriggerServerEvent('blanchi:stestcop')
								local pourcent = (100/4)*(1.7^nbPolice)
								local policier = nbPolice
								blanchiment = true
								if pourcent > 100 then pourcent = 100 end
								if timerfinish==false then
									AddTextComponentString("~g~Blanchiment en cours a "..pourcent.." pourcent avec "..policier.." Policiers en Services")
									DrawSubtitleTimed(1000, 1)
								end
								if start == false then
									TriggerEvent("blanchi:start")
								end
								Citizen.Wait(5)
								if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), 595.945,-457.364,24.8388, true) <= 4 and timerfinish==true then
									TriggerServerEvent('blanchi:transform',policier)
									--Citizen.Trace(policier)
									checksale = false
									blanchiment = false
									secondsRemaining = 0
									timerfinish = false
									start=false
								end
							end
						--else
						--	TriggerEvent("mt:missiontext", 'Il faut au moins 4 policiers en Service pour Blanchir', 800)
						--end
					end
				end
		end

    end
end)

Citizen.CreateThread(function()
	while true do
		if blanchiment then
			Citizen.Wait(1000)
			if(secondsRemaining > 0)then
				secondsRemaining = secondsRemaining - 1
				if secondsRemaining == 0 then
					blanchiment = false
					timerfinish = true
					Citizen.Wait(2000)
					timerfinish = false
					start=false
				end
			end
		end
		Citizen.Wait(0)
	end
end)

Citizen.CreateThread(function()
	while true do
		if blanchiment then

			drawTxt(0.66, 1.44, 1.0,1.0,0.4, "Blanchiment : ~r~" .. secondsRemaining .. "~w~ secondes restantes", 255, 255, 255, 255)
			DrawMarker(1, 595.945, -457.364, 24.8388 - 1, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 0.2, 1555, 0, 0,255, 0, 0, 0,0)
		end
		Citizen.Wait(0)
	end
end)

-- Citizen.CreateThread(function()
	-- while true do
		-- Citizen.Wait(90000)
		-- TriggerServerEvent('blanchi:stestcop')
	-- end
-- end)

RegisterNetEvent('blanchi:getcop')
AddEventHandler("blanchi:getcop", function(nbPolicier)
	nbPolice = nbPolicier
	--Citizen.Trace(nbPolice)
end)
RegisterNetEvent('blanchi:start')
AddEventHandler("blanchi:start", function()
	start = true
	secondsRemaining = 90
	--Citizen.Trace(nbPolice)
end)

function drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
	    SetTextOutline()
	end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end
--------------------------------------------------------------------------------
--train , ouais WTF, mais bon, what does the fox say ?
local thisTrain
local trainCoords = {}
local storedTrains = {}

Citizen.CreateThread(function()
  -- Define the train models
  local trainModels = {"freight", "freightcar", "freightgrain", "freightcont1", "freightcont2","tankercar", "freighttrailer", "metrotrain"}

  -- Define the train coordinates
  local trains = {
    {type=0, x=-498.4123, y=4304.3, z=88.40305},
    {type=0, x=2324.3, y=2670.7, z=44.45},
    {type=0, x=1063.595, y=3227.571, z=39.3899},
    {type=7, x=2928.826, y=3572.775, z=54.0699},
    {type=15, x=217.616, y=-2215.75, z=11.6666},
    {type=22, x=1800.49, y=3504.19, z=37.9},
    {type=24, x=181.1, y=-1198.8, z=37.6},
  }

  -- Load the train models
  for i= 1, 8 do
    RequestModel(GetHashKey(trainModels[i]))
    while not HasModelLoaded(GetHashKey(trainModels[i])) do
      Citizen.Wait(1)
    end
  end

 -- Load the traindriver
  RequestModel(GetHashKey("s_m_m_lsmetro_01"))
  while not HasModelLoaded(GetHashKey("s_m_m_lsmetro_01")) do
    Citizen.Wait(1)
  end

  -- Start the trains
  for i=1, 7, 1 do
    local trainCoords = trains[i]
    local thisTrain = CreateMissionTrain(trainCoords.type, trainCoords.x, trainCoords.y, trainCoords.z, true)
    SetEntityProofs(thisTrain, true, true, true, true, true, false, 0, false)
    SetEntityAsMissionEntity(thisTrain, true, true)
    CreatePedInsideVehicle(thisTrain, 26, GetHashKey("s_m_m_lsmetro_01"), -1, 1, true)
    table.insert(storedTrains, {train = thisTrain})
  end
end)
