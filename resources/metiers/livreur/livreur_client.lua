	local livreur_blipsTemp
	local livreur_markerBool = false
	local isInServiceLivreur = false
	
	local vehiculeChargeur = nil
	local cargaisonChargeur = nil
	local cargaisonAttachedToChargeur = false

	local vehiculeLivreur = nil
	local cargaisonLivreur = nil
	local cargaisonAttachedToLivreur = false
	local cargaisonLivreurBlip = nil

	function livreur_callSE(evt)
		Menu.hidden = not Menu.hidden
		Menu.renderGUI()
		TriggerServerEvent(evt)
	end
	
	function metierGarage1()
		MenuTitle = "Vehicule"
		ClearMenu()
		Menu.addButton("Livreur", "myGetCarLivreur", nil)
	end
	
	function metierGarage2()
		MenuTitle = "Vehicule"
		ClearMenu()
		Menu.addButton("Chargeur", "myGetCarChargeur", nil)
	end
	
	function myCreateVehicule(vehiculeToCreate, vehiculeType, x, y, z, radius, cargaisonAttachedTo, cargaison)
		if (vehiculeToCreate == nil) then
			local vehiculeDetected = GetClosestVehicle(x, y, z, 3.000, 0, 70)
			if not DoesEntityExist(vehiculeDetected) then
				RequestModel(vehiculeType)
				while not HasModelLoaded(vehiculeType) do
					Wait(1)
				end
				local plate = math.random(100, 900)
				-- local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
				vehiculeToCreate = CreateVehicle(vehiculeType, x, y, z, radius, true, false)
				SetEntityInvincible(vehiculeToCreate, false)
				SetVehicleOnGroundProperly(vehiculeToCreate)
				-- local VehiculeId = NetworkGetNetworkIdFromEntity(vehiculeToCreate)
				-- NetworkRequestControlOfNetworkId(VehiculeId)
				-- SetNetworkIdCanMigrate(VehiculeId, true)
				-- SetModelAsNoLongerNeeded(vehiculeType)
				SetVehicleOnGroundProperly(vehiculeToCreate)
				SetVehicleNumberPlateText(vehiculeToCreate, "LIVR"..plate.." ")			
				SetVehicleHasBeenOwnedByPlayer(vehiculeToCreate, true)	
			else
				TriggerEvent("chatMessage", 'Livreur', { 0, 255, 255}, "Zone encombrée.")
			end
		else
			if IsEntityAttachedToEntity(cargaison, vehiculeToCreate) then
				DeleteObject(cargaison)
				cargaison = nil
				cargaisonAttachedTo = false
			end
			SetEntityAsMissionEntity(vehiculeToCreate, true, true)
			SetVehicleHasBeenOwnedByPlayer(vehiculeToCreate, false)
			Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehiculeToCreate))
			vehiculeToCreate = nil
		end

		return vehiculeToCreate, cargaisonAttachedTo, cargaison
	end
	
	function myGetCarChargeur()
		Menu.hidden = not Menu.hidden
		Menu.renderGUI()
		local vehiculeToLoad = vehiculeChargeur
		local cargaison = cargaisonChargeur
		local cargaisonIsAttached = cargaisonAttachedToChargeur
	
		vehiculeChargeur, cargaisonAttachedToChargeur, cargaisonChargeur = myCreateVehicule(vehiculeToLoad, livreur_garage["Chargeur"].model, livreur_garage["Chargeur"].x, livreur_garage["Chargeur"].y, livreur_garage["Chargeur"].z, livreur_garage["Chargeur"].radius, cargaisonIsAttached, cargaison)
	end
	
	function myGetCarLivreur()
		Menu.hidden = not Menu.hidden
		Menu.renderGUI()
		local vehiculeToLoad = vehiculeLivreur
		local cargaison = cargaisonLivreur
		local cargaisonIsAttached = cargaisonAttachedToLivreur

		vehiculeLivreur, cargaisonAttachedToLivreur, cargaisonLivreur = myCreateVehicule(vehiculeToLoad, livreur_garage["Livreur"].model, livreur_garage["Livreur"].x, livreur_garage["Livreur"].y, livreur_garage["Livreur"].z, livreur_garage["Livreur"].radius, cargaisonIsAttached, cargaison)
	end

	function myPlayerIsInVehiculeType(vehiculeModel)
		local vehiclePed = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		local vehicleHashModel = GetHashKey(vehiculeModel)
		local isInVehicle = IsVehicleModel(vehiclePed, vehicleHashModel)

		if (isInVehicle) then
			return true
		else
			return false
		end
		return false
	end
	
	function SpawnCargaison()
		local obj = GetHashKey('prop_air_cargo_04c')
		RequestModel(obj)
		while not HasModelLoaded(obj) do
			Wait(1)
		end
		local cargaisonTemp = GetClosestObjectOfType(livreur_blips["Contener"].x, livreur_blips["Contener"].y, livreur_blips["Contener"].z, 3.000, obj, 0, 1, 1)	
		if DoesEntityExist(cargaisonTemp) then
			local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonTemp)
			NetworkRequestControlOfNetworkId(ObjectId)
		
			DeleteObject(cargaisonTemp)
			-- livreur_blips["Contener"].x = livreur_blips["Contener"].x + 5.0
			Citizen.InvokeNative( 0x539E0AE3E6634B9F, Citizen.PointerValueIntInitialized(cargaisonTemp))
			
			TriggerEvent("chatMessage", 'Livreur', { 0, 255, 255}, "Zone encombrée. Cargaison supprimée.")
		
		else
			if DoesEntityExist(cargaisonLivreur) then
				-- local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonLivreur)
				-- NetworkRequestControlOfNetworkId(ObjectId)
				DeleteObject(cargaisonLivreur)
				
				TriggerEvent("chatMessage", 'Livreur', { 0, 255, 255}, "Cargaison Livreur supprimée.")
			end
			if DoesEntityExist(cargaisonChargeur) then
				-- local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonChargeur)
				-- NetworkRequestControlOfNetworkId(ObjectId)
				DeleteObject(cargaisonChargeur)
			
				TriggerEvent("chatMessage", 'Livreur', { 0, 255, 255}, "Cargaison Chargeur supprimée.")
			end
			
			cargaisonLivreur = nil
			cargaisonAttachedToLivreur = false
			cargaisonChargeur = nil
			cargaisonAttachedToChargeur = false
			
			cargaisonLivreur = CreateObjectNoOffset("prop_air_cargo_04c", livreur_blips["Contener"].x, livreur_blips["Contener"].y, livreur_blips["Contener"].z, true, true, true)
			local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonLivreur)
			PlaceObjectOnGroundProperly(cargaisonLivreur)
			SetEntityRotation(cargaisonLivreur, 0.0, 0.0, 45.0, 0, true)
			
			cargaisonLivreurBlip = AddBlipForEntity(cargaisonLivreur)
			SetBlipSprite(cargaisonLivreurBlip, 351)
			SetBlipAsShortRange(cargaisonLivreurBlip, true)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString("Cargaison")
			EndTextCommandSetBlipName(cargaisonLivreurBlip)
			
			SetNetworkIdExistsOnAllMachines(ObjectId, true)
			SetNetworkIdCanMigrate(ObjectId, true)
			SetEntityInvincible(cargaisonLivreur, false)
			SetEntityDynamic(cargaisonLivreur, true)
			TriggerEvent("chatMessage", 'Livreur', { 0, 255, 255}, "Cargaison crée.")
		
		end
		
	end
	
	function attacheCargaison()
		if (myPlayerIsInVehiculeType('forklift')) then 
			local vehicleToAttach = GetVehiclePedIsIn(GetPlayerPed(-1), true)
			-- local VehiculeId = NetworkGetNetworkIdFromEntity(vehicleToAttach)
			-- NetworkRequestControlOfNetworkId(VehiculeId)
			local obj = GetHashKey('prop_air_cargo_04c')
			RequestModel(obj)
			while not HasModelLoaded(obj) do
				Wait(1)
			end
			local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
			local cargaisonTemp = GetClosestObjectOfType(livreur_blips["Contener"].x, livreur_blips["Contener"].y, livreur_blips["Contener"].z, 3.000, obj, 0, 1, 1)	
			if DoesEntityExist(cargaisonTemp) then
				ClearPrints()
				SetTextEntry_2("STRING")
				AddTextComponentString("Appuyez sur 'Utiliser / E' pour charger la ~g~cargaison~s~")
				DrawSubtitleTimed(2000, 1)
				if IsControlJustPressed(1, 51) then			
					-- local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonTemp)
					-- if NetworkDoesNetworkIdExist(ObjectId) then
						-- if NetworkHasControlOfNetworkId(ObjectId) then
							-- NetworkRequestControlOfNetworkId(ObjectId)
						-- end	
					-- end
					-- cargaisonChargeur = NetworkGetEntityFromNetworkId(ObjectId)
					cargaisonChargeur = cargaisonTemp
					AttachEntityToEntity(cargaisonChargeur, vehicleToAttach, 5, 0.3, 0.8, 0.0, 0.0, 0.0, 0.0, false, false, false, false, 5, true)
					-- AttachEntityToEntity(cargaisonChargeur, vehicleToAttach, 15, 0.0, 2.0, -1.7, 0.0, 0.0, 0.0, false, false, false, false, 20, true)--handler
					cargaisonAttachedToChargeur = not cargaisonAttachedToChargeur
				end
			else	
				ClearPrints()
				SetTextEntry_2("STRING")
				AddTextComponentString("Aucune ~g~cargaison~s~ a charger.")
				DrawSubtitleTimed(2000, 1)
			end
		else
			ClearPrints()
			SetTextEntry_2("STRING")
			AddTextComponentString("Vous n'avez pas le bon vehicule.")
			DrawSubtitleTimed(2000, 1)
		end
	end

	function switchVehiculeCargaison()
		local vehiculeLivreurDetection = GetClosestVehicle(livreur_blips["Remorque"].x, livreur_blips["Remorque"].y, livreur_blips["Remorque"].z, 3.000, 0, 70)
		if DoesEntityExist(vehiculeLivreurDetection) then
			local flatmodel = GetHashKey('flatbed')
			local isVehicleFlat = IsVehicleModel(vehiculeLivreurDetection, flatmodel)
			if (isVehicleFlat) then
				if IsEntityAttachedToEntity(cargaisonChargeur, vehiculeLivreurDetection) then
					ClearPrints()
					SetTextEntry_2("STRING")
					AddTextComponentString("Vehicule Chargé.")
					DrawSubtitleTimed(2000, 1)
				else
					ClearPrints()
					SetTextEntry_2("STRING")
					AddTextComponentString("Appuyez sur 'Utiliser / E' pour décharger la cargaison sur le vehicule")
					DrawSubtitleTimed(2000, 1)
					
					if IsControlJustPressed(1, 51) then
						-- local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonTemp)
						-- if NetworkDoesNetworkIdExist(ObjectId) then
							-- if NetworkHasControlOfNetworkId(ObjectId) then
								-- NetworkRequestControlOfNetworkId(ObjectId)
							-- end	
						-- end
						DetachEntity(cargaisonChargeur, true, true)
						AttachEntityToEntity(cargaisonChargeur, vehiculeLivreurDetection, 15, 0.0, -3.7, -1.3, 0.0, 0.0, 0.0, false, false, false, false, 20, true)
						cargaisonAttachedToChargeur = not cargaisonAttachedToChargeur
						cargaisonAttachedToLivreur = not cargaisonAttachedToLivreur
						cargaisonChargeur = nil
						local salaire = math.random(10, 30)
						TriggerServerEvent('mission:completed', salaire)
					end
				end
			else
				ClearPrints()
				SetTextEntry_2("STRING")
				AddTextComponentString("Aucun vehicule pour décharger.")
				DrawSubtitleTimed(2000, 1)
			end
		else
			ClearPrints()
			SetTextEntry_2("STRING")
			AddTextComponentString("Aucun vehicule pour décharger.")
			DrawSubtitleTimed(2000, 1)
		end
	end
	
	RegisterNetEvent('livreur:drawBlips')
	AddEventHandler('livreur:drawBlips', function () 
		for key, item in pairs(livreur_blips) do
			item.blip = AddBlipForCoord(item.x, item.y, item.z)
			SetBlipSprite(item.blip, item.id)
			SetBlipAsShortRange(item.blip, true)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString(key)
			EndTextCommandSetBlipName(item.blip)
		end
		livreur_blipsTemp = livreur_blips
	end)

	RegisterNetEvent('livreur:deleteBlips')
	AddEventHandler('livreur:deleteBlips', function ()
		livreur_markerBool = false
		for _, item in pairs(livreur_blipsTemp) do
			RemoveBlip(item.blip)
		end
	end)

	RegisterNetEvent('livreur:drawMarker')
	AddEventHandler('livreur:drawMarker', function (boolean) 
		livreur_markerBool = boolean
	end)

	RegisterNetEvent('livreur:marker')
	AddEventHandler('livreur:marker', function ()
		Citizen.CreateThread(function () 
			while livreur_markerBool == true do
				Citizen.Wait(1)			
				if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Entreprise"].x,livreur_blips["Entreprise"].y,livreur_blips["Entreprise"].z, true) <= livreur_blips["Entreprise"].distanceMarker then
					DrawMarker(1,livreur_blips["Entreprise"].x,livreur_blips["Entreprise"].y,livreur_blips["Entreprise"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
					if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Entreprise"].x,livreur_blips["Entreprise"].y,livreur_blips["Entreprise"].z, true) <= livreur_blips["Entreprise"].distanceBetweenCoords then
						ClearPrints()
						SetTextEntry_2("STRING")
						AddTextComponentString("Appuyez sur 'Utiliser / E'pour prendre votre service")
						DrawSubtitleTimed(2000, 1)
						if IsControlJustPressed(1, 51) then
							GetServiceLivreur()
							isInService = not isInService
							if (vehiculeChargeur ~= nil) then
								SetEntityAsMissionEntity(vehiculeChargeur, true, true)
								Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehiculeChargeur))
								vehiculeChargeur = nil
							end
							if (vehiculeLivreur ~= nil) then
								SetEntityAsMissionEntity(vehiculeLivreur, true, true)
								Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehiculeLivreur))
								vehiculeLivreur = nil
							end
							if DoesEntityExist(cargaisonLivreur) then
								-- local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonLivreur)
								-- NetworkRequestControlOfNetworkId(ObjectId)
								DeleteObject(cargaisonLivreur)								
							end
							if DoesEntityExist(cargaisonChargeur) then
								-- local ObjectId = NetworkGetNetworkIdFromEntity(cargaisonChargeur)
								-- NetworkRequestControlOfNetworkId(ObjectId)
								DeleteObject(cargaisonChargeur)
							end
							cargaisonLivreur = nil
							cargaisonAttachedToLivreur = false
							cargaisonChargeur = nil
							cargaisonAttachedToChargeur = false
						end
					end
				end
				if (isInService) then 
					if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Commande"].x, livreur_blips["Commande"].y, livreur_blips["Commande"].z, true) <= livreur_blips["Commande"].distanceMarker then
						DrawMarker(1, livreur_blips["Commande"].x, livreur_blips["Commande"].y,livreur_blips["Commande"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Commande"].x, livreur_blips["Commande"].y, livreur_blips["Commande"].z, true) <= livreur_blips["Commande"].distanceBetweenCoords then
							ClearPrints()
							SetTextEntry_2("STRING")
							AddTextComponentString("Appuyez sur 'Utiliser / E' pour commander ou annuler une ~g~cargaison~s~ a livrer.")
							DrawSubtitleTimed(2000, 1)
							if IsControlJustPressed(1, 51) then
								SpawnCargaison()
							end
						end
					elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Garage1"].x, livreur_blips["Garage1"].y, livreur_blips["Garage1"].z, true) <= livreur_blips["Garage1"].distanceMarker then
						DrawMarker(1, livreur_blips["Garage1"].x, livreur_blips["Garage1"].y,livreur_blips["Garage1"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Garage1"].x, livreur_blips["Garage1"].y, livreur_blips["Garage1"].z, true) <= livreur_blips["Garage1"].distanceBetweenCoords then
							ClearPrints()
							SetTextEntry_2("STRING")
							AddTextComponentString("Appuyez sur 'Utiliser / E' pour faire appairaitre ou ranger votre ~b~vehicule")
							DrawSubtitleTimed(2000, 1)
							if IsControlJustPressed(1, 51) then
								metierGarage1()
								Menu.hidden = not Menu.hidden
							end
						end
					elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Garage2"].x, livreur_blips["Garage2"].y, livreur_blips["Garage2"].z, true) <= livreur_blips["Garage2"].distanceMarker then
						DrawMarker(1, livreur_blips["Garage2"].x, livreur_blips["Garage2"].y,livreur_blips["Garage2"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Garage2"].x, livreur_blips["Garage2"].y, livreur_blips["Garage2"].z, true) <= livreur_blips["Garage2"].distanceBetweenCoords then
							ClearPrints()
							SetTextEntry_2("STRING")
							AddTextComponentString("Appuyez sur 'Utiliser / E' pour faire appairaitre ou ranger votre ~b~vehicule")
							DrawSubtitleTimed(2000, 1)
							if IsControlJustPressed(1, 51) then
								metierGarage2()
								Menu.hidden = not Menu.hidden
							end
						end
					elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Contener"].x, livreur_blips["Contener"].y, livreur_blips["Contener"].z, true) <= livreur_blips["Contener"].distanceMarker then
						DrawMarker(1, livreur_blips["Contener"].x, livreur_blips["Contener"].y, livreur_blips["Contener"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Contener"].x, livreur_blips["Contener"].y, livreur_blips["Contener"].z, true) <= livreur_blips["Contener"].distanceBetweenCoords then	
							if not cargaisonAttachedToChargeur then
								attacheCargaison()			
							end
						end	
					elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Remorque"].x, livreur_blips["Remorque"].y, livreur_blips["Remorque"].z, true) <= livreur_blips["Remorque"].distanceMarker then
						DrawMarker(1, livreur_blips["Remorque"].x, livreur_blips["Remorque"].y, livreur_blips["Remorque"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
						if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), livreur_blips["Remorque"].x, livreur_blips["Remorque"].y, livreur_blips["Remorque"].z, true) <= livreur_blips["Remorque"].distanceBetweenCoords then
							if cargaisonAttachedToChargeur then
								if (myPlayerIsInVehiculeType('forklift')) then 
									switchVehiculeCargaison()
								end
							else		
								if (myPlayerIsInVehiculeType('flatbed')) then 
									ClearPrints()
									SetTextEntry_2("STRING")
									AddTextComponentString("Garez ici votre vehicule de Livraison pour charger une ~g~cargaison~s~.")
									DrawSubtitleTimed(2000, 1)
								end
							end
						end
					elseif (cargaisonLivreur ~= nil) then
						if (myPlayerIsInVehiculeType('flatbed')) then
							if GetDistanceBetweenCoords(GetEntityCoords(cargaisonLivreur), livreur_blips["depot"].x, livreur_blips["depot"].y, livreur_blips["depot"].z, true) <= livreur_blips["depot"].distanceMarker then
								DrawMarker(1, livreur_blips["depot"].x, livreur_blips["depot"].y, livreur_blips["depot"].z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
								ClearPrints()
								SetTextEntry_2("STRING")
								AddTextComponentString("Appuyez sur 'Utiliser / E' pour livrer votre ~g~cargaison~s~.")
								DrawSubtitleTimed(2000, 1)
								if IsControlJustPressed(1, 51) then
									DeleteObject(cargaisonLivreur)
									cargaisonLivreur = nil
									cargaisonAttachedToLivreur = not cargaisonAttachedToLivreur
									local salaire = math.random(200, 300)
									TriggerServerEvent('mission:completed', salaire)
								end
							end
						end
					else
						if not Menu.hidden then
							Menu.hidden = not Menu.hidden
						end
					end
					Menu.renderGUI()
				end
			end
		end)
	end)
		
	function GetServiceLivreur()
		local playerPed = GetPlayerPed(-1)
		if isInServiceLivreur then
			notif("Vous n\'êtes plus en service")
			TriggerServerEvent("skin_customization:SpawnPlayer")
		else
			notif("Début du service")
			TriggerEvent("livreur:getSkin")
		end
		isInServiceLivreur = not isInServiceLivreur
		TriggerServerEvent('livreur:setService', isInServiceLivreur)
	end
	
	RegisterNetEvent('livreur:getSkin')	
	AddEventHandler('livreur:getSkin', function (source)
		local hashSkin = GetHashKey("mp_m_freemode_01")
		Citizen.CreateThread(function()
		if(GetEntityModel(GetPlayerPed(-1)) == hashSkin) then
			SetPedComponentVariation(GetPlayerPed(-1), 11, 41, 0, 2)  -- Top
			SetPedComponentVariation(GetPlayerPed(-1), 8, 59, 0, 2)   -- under coat
			SetPedComponentVariation(GetPlayerPed(-1), 4, 7, 0, 2)   -- Pants
			SetPedComponentVariation(GetPlayerPed(-1), 6, 25, 0, 2)   -- shoes
			SetPedComponentVariation(GetPlayerPed(-1), 3, 66, 0, 2)   -- under skin
		else
			SetPedComponentVariation(GetPlayerPed(-1), 11, 109, 0, 2)  -- Top
			SetPedComponentVariation(GetPlayerPed(-1), 8, 1, 0, 2)   -- under coat
			SetPedComponentVariation(GetPlayerPed(-1), 4, 45, 2, 2)   -- Pants
			SetPedComponentVariation(GetPlayerPed(-1), 6, 36, 0, 2)   -- shoes
			SetPedComponentVariation(GetPlayerPed(-1), 3, 6, 0, 2)   -- under skin
		end
		end)
		--TriggerServerEvent("skin_customization:SpawnPlayer")
		--RemoveAllPedWeapons(GetPlayerPed(-1), true)
	end)
	

	
