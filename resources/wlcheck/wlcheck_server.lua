--[[---------------------------------------------------------------------------------
||                                                                                  ||
||                      WHITELIST CHECKING SCRIPT - GTA5 - FiveM                    ||
||                                   Author = Shedow                                ||
||                            Created for N3MTV community                           ||
||                                                                                  ||
----------------------------------------------------------------------------------]]--
require "resources/mysql-async/lib/MySQL"

function getPlayerID(source)
	local identifiers = GetPlayerIdentifiers(source)
	local player = getIdentifiant(identifiers)
	return player
end

function getIdentifiant(id)
	for _, v in ipairs(id) do
		return v
	end
end

AddEventHandler('playerConnecting', function()
	local player = getPlayerID(source)

	MySQL.Async.fetchScalar("SELECT identifier FROM whitelist WHERE identifier='@id'", { ['@id'] = player }, function(result)
		if not result[1] then
			DropPlayer(source, "You are not whitelisted :P")
			print(player.." was kicked : not whitelisted !")
			CancelEvent()
		end
	end)

end)