
local positionParachute={
    x=454.489959716797,y=5584.47998046875,z=780.191284179688
}

local positionParachute2={
	x=-80.7721328735352,y=-825.642822265625,z=326.083953857422
}

function giveParachute()
    GiveWeaponToPed(GetPlayerPed(-1), GetHashKey("GADGET_PARACHUTE"), 150, true, true)
end

function IsNear()
  if(GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), positionParachute.x, positionParachute.y, positionParachute.z, true) <= 5) then
      DrawMarker(1, positionParachute.x, positionParachute.y, positionParachute.z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
  end
  if(GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), positionParachute.x, positionParachute.y, positionParachute.z, true) <= 2) then
      return true
  end

  if(GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), positionParachute2.x, positionParachute2.y, positionParachute2.z, true) <= 5) then
      DrawMarker(1, positionParachute2.x, positionParachute2.y, positionParachute2.z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
  end
  if(GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), positionParachute2.x, positionParachute2.y, positionParachute2.z, true) <= 2) then
      return true
  end
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(2)
        --DrawMarker(1, positionParachute.x, positionParachute.y, positionParachute.z, 0, 0, 0, 0, 0, 0, 2.001, 2.0001, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
        if (IsNear() == true) then
            ClearPrints()
            SetTextEntry_2("STRING")
            AddTextComponentString("Appuyez sur ~r~E~w~ pour obtenir un ~b~parachute")
            DrawSubtitleTimed(2000, 1)
            if IsControlJustPressed(1, 51) then
                giveParachute()
            end
        end
    end
    end
)
